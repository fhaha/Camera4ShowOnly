
#include "nci_fhaha_testcamera_mySurfaceView.h"

#include "jniSurfaceFunc.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <getopt.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>
#include <dirent.h>
#include <linux/videodev2.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>



#define CLEAR(x) memset (&(x), 0, sizeof (x))
#define _FHTEST	0

#if _FHTEST

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,__VA_ARGS__)
#else

#define LOGD(...) do{}while(0)
#endif

#define LOGINFO(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

struct buffer {
    void *start;
    size_t length;
};



//static char * dev_name = "/dev/video0";//摄像头设备名
static int fd[4] = {-1,-1,-1,-1};
struct buffer *buffers[4] = {NULL,NULL,NULL,NULL};
static unsigned int n_buffers[4] = {0};
static unsigned char * disbuf[4] = {NULL};
static int socks[4]={0};
static struct sockaddr_in addrto[4];
int MYWIDTH[UCCOUNT] = {320,320,320,320};
int MYHEIGHT[UCCOUNT] = {240,240,240,240};

JNIEXPORT jint
JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doVideoRegister
        (JNIEnv * env, jobject obj1 ,jint index ,jobject jsurface )
{
	LOGINFO("%s with index %d", __func__,index);
	setSurface(env, jsurface,17,index);
	return 0 ;
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doVideoUnregister
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_nci_fhaha_testcamera_mySurfaceView_doVideoUnregister(JNIEnv* env,jobject obj,jint index)
{
	LOGINFO("%s with index %d", __func__,index);

}

int createSockets(int index)
{
	
	if ((socks[index] = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{   
		LOGINFO("create socket for %d failed",index);
		return -1;
	}   
	else
		LOGINFO("create socket for %d success",index);
	const int opt = 1;
	//设置该套接字为广播类型，
	int nb = 0;
	//nb = setsockopt(socks[index], SOL_SOCKET, SO_BROADCAST, (char *)&opt, sizeof(opt));
	//if(nb == -1)
	//{
	//	return -1;
	//}

	bzero(&addrto[index], sizeof(struct sockaddr_in));
	addrto[index].sin_family=AF_INET;
	//addrto[index].sin_addr.s_addr=htonl(INADDR_BROADCAST);
	addrto[index].sin_addr.s_addr = inet_addr("192.168.100.1"); 
	addrto[index].sin_port=htons(6000+index);
	if(index>3)
		socks[index]=-1;
	else if(connect(socks[index],(struct sockaddr *)(&addrto[index]),sizeof(struct sockaddr))==-1)
	{
		LOGINFO("Connect error:%s",strerror(errno));
		socks[index]=-1;
	}
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_setDataSource
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint
JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1setDataSource
        (JNIEnv * env, jobject obj,jint index,jstring str, jint width, jint height
)
{
	LOGINFO("%s with index %d", __func__,index);
#if FOR_CAPTURE
	MYWIDTH[index]= DEFWIDTH;
	MYHEIGHT[index] = DEFHEIGHT ;
#else
	MYWIDTH[index]= (width>>1) << 1;
	MYHEIGHT[index] = (height>>1) << 1 ;
#endif
	LOGINFO("%s with index %d width %d height %d", __func__,index,MYWIDTH[index],MYHEIGHT[index]);
	if(disbuf[index]!=NULL){
		free(disbuf[index]);
		disbuf[index]=NULL;
	}
	#if FOR_CAPTURE
	
		disbuf[index] = (unsigned char *)malloc(MYWIDTH[index] * MYHEIGHT[index] *3);
		#if SENDTONET
		createSockets(index);
		#endif
	#else
	disbuf[index] = (unsigned char *)malloc(MYWIDTH[index] * MYHEIGHT[index] *4);
	#endif
	return 0;
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_prepare
 * Signature: ()I
 */
JNIEXPORT jint
JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1prepare
        (JNIEnv * env, jobject obj,jint index)
{
	LOGINFO("%s with index %d", __func__,index);
	
	return 0;
}
static char *devname[4] = {"/dev/video0","/dev/video1","/dev/video2","/dev/video3"};
/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_start
 * Signature: ()I
 */
JNIEXPORT jint
JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1start(JNIEnv * env, jobject obj,jint index)
{
	LOGD("%s with index %d", __func__,index);
	struct v4l2_capability cap;
	struct v4l2_format fmt;
	struct v4l2_fmtdesc efmt;
	struct v4l2_cropcap cropcap;
	struct v4l2_crop crop;
	
	enum v4l2_buf_type type;
	v4l2_std_id vstd;
	int tmpret = 0;
	int ispal = 0, isntsc = 0;
	int ismjpg = 0;
	int isyuyv = 0;
	
	LOGD("want open %s",devname[index]);
	fd[index] = open(devname[index], O_RDWR /* required */ | O_NONBLOCK, 0);//打开设备
	if(fd[index] == -1){
		LOGINFO("open dev %s failed", devname[index]);
		return -1;
	}
	
	ioctl (fd[index], VIDIOC_QUERYCAP, &cap);//获取摄像头参数
	
	LOGD("Driver Name:%s\nCard Name:%s\nBus info:%s\nDriver Version:%u.%u.%u\n", cap.driver,
	        cap.card, cap.bus_info,
	        (cap.version >> 16) & 0XFF, (cap.version >> 8) & 0XFF, cap.version & 0XFF);
	
	LOGD("Capibility:%x\n", cap.capabilities);
	
	if((cap.capabilities &V4L2_CAP_VIDEO_CAPTURE)==0){
		LOGINFO("unsupport video capture\n");
		return -2;
	}
	if(0==cap.capabilities &V4L2_CAP_STREAMING){
		LOGINFO("unsupport streaming io\n");
		return -3;
	}
	
	do{
		tmpret = ioctl(fd[index], VIDIOC_QUERYSTD, &vstd);
	}while(tmpret == -1 && errno == EAGAIN);
	switch(vstd){
		case V4L2_STD_PAL:
			LOGINFO("support PAL\n");
			ispal = 1;
			break;
		case V4L2_STD_NTSC:
			LOGINFO("support NTSC\n");
			isntsc = 1;
			break;
	}
	
	CLEAR(efmt);
	efmt.index = 0;
	efmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	while(tmpret = ioctl(fd[index], VIDIOC_ENUM_FMT, &efmt) == 0){
		LOGD("index %d :\n\tpixelformat:'%c%c%c%c',description = '%s'\n",
		        efmt.index, efmt.pixelformat & 0xff, (efmt.pixelformat >> 8) & 0xff,
		        (efmt.pixelformat >> 16) & 0xff, (efmt.pixelformat >> 24) & 0xff,
		        efmt.description);
		if(memcmp((void *)&efmt.pixelformat,"MJPG",4)==0){
			ismjpg = 1;
		}
		if(memcmp((void *)&efmt.pixelformat,"YUYV",4)==0){
			isyuyv = 1;
		}
		efmt.index++;
	}
	CLEAR(cropcap);
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	tmpret = ioctl(fd[index], VIDIOC_CROPCAP, &cropcap);
	if( -1 == tmpret){
		LOGINFO("call cropcap failed!\n");
		return -4;
	}
	LOGD(" cropcap bound:%d %d %d %d\ndefrect:%d %d %d %d\nfract %d %d\n",
	        cropcap.bounds.left, cropcap.bounds.top, cropcap.bounds.width, cropcap.bounds.height,
	        cropcap.defrect.left, cropcap.defrect.top, cropcap.defrect.width, cropcap.defrect.height,
	        cropcap.pixelaspect.numerator, cropcap.pixelaspect.denominator);
	
	CLEAR (fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	//fmt.fmt.pix.width = cropcap.defrect.width;
	fmt.fmt.pix.width = MYWIDTH[index];
	//fmt.fmt.pix.height = cropcap.defrect.height;
	fmt.fmt.pix.height = MYHEIGHT[index];
	fmt.fmt.pix.pixelformat = isyuyv ? V4L2_PIX_FMT_YUYV : V4L2_PIX_FMT_YUYV;
	fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
	
	tmpret = ioctl(fd[index], VIDIOC_S_FMT, &fmt); //设置图像格式

	LOGD("set %d fmt ret %d,bytes per line %d ,height %d\n", index,tmpret, fmt.fmt.pix.bytesperline,
        fmt.fmt.pix.height);
  if(tmpret != 0){
  	return -5;
  }
	
	
	//file_length = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height; //计算图片大小
	
	struct v4l2_requestbuffers req;
	CLEAR (req);
	req.count = 1;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;
	
	tmpret = ioctl(fd[index], VIDIOC_REQBUFS, &req); //申请缓冲，count是申请的数量
	LOGD("REQBUFS ret %d count %d\n", tmpret, req.count);
	if (req.count < 1){
		LOGINFO("Insufficient buffer memory");
		return -6;
	}

	buffers[index] = (struct buffer *) calloc(req.count, sizeof(*buffers[index]));//内存中建立对应空间

	for (n_buffers[index] = 0;n_buffers[index]<req.count;++n_buffers[index])
	{
		struct v4l2_buffer buf; //驱动中的一帧
		CLEAR (buf);
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = n_buffers[index];
		
		if (-1 ==	ioctl (fd[index], VIDIOC_QUERYBUF, &buf	)){ //映射用户空间
			LOGINFO ("VIDIOC_QUERYBUF error\n");
			break;
		}
		buffers[index][n_buffers[index]].length = buf.length;
		buffers[index][n_buffers[index]].start = mmap(NULL /* start anywhere */, //通过mmap建立映射关系
		             buf.length,
		             PROT_READ | PROT_WRITE /* required */,
		             MAP_SHARED /* recommended */,
		             fd[index], buf.m.offset);
		
		if (MAP_FAILED == buffers[index][n_buffers[index]].start){
			LOGINFO ("mmap in %d failed for %d", index,errno);
			break;
		}
	
	}
	if(n_buffers[index] != req.count){
		LOGINFO("mmap all buffer failed at %d", n_buffers[index]);
		free(buffers[index]);
		return -7;
	}
	LOGD("want mmap");
	for (int	j = 0;	j<n_buffers[index];	++j)	{
		struct v4l2_buffer buf;
		CLEAR (buf);
		
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = j;
		
		if (-1 ==	ioctl (fd[index], VIDIOC_QBUF, &buf	)){//申请到的缓冲进入列队
			LOGINFO ("VIDIOC_QBUF failed in %d",j);
			return -8;
		}
	}
	LOGD("want streamon ");
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	
	if (-1 ==	ioctl (fd[index], VIDIOC_STREAMON, &type)){ //开始捕捉图像数据
		LOGINFO ("VIDIOC_STREAMON failed in %d",index);
		return -9;
	}
	
	return 0;
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1exit(JNIEnv* env,jobject obj,jint index)
{
	LOGD("%s with index %d", __func__,index);

	int i = 0;
	
	enum v4l2_buf_type  v4l2type;  
	v4l2type = V4L2_BUF_TYPE_VIDEO_CAPTURE;   
	int tret = ioctl(fd[index], VIDIOC_STREAMOFF, &v4l2type);
	LOGINFO("%d VIDIOC_STREAMOFF ret %d",tret);

	if(fd[index]!=-1){
		LOGINFO("close %d :%d\n",index,fd[index]);
		close(fd[index]);
		fd[index]=-1;
	}
	
	if(disbuf[index]!=NULL)
	{
		LOGINFO("free disbuf %p",disbuf[index]);
		free(disbuf[index]);
		disbuf[index]=NULL;
	}
	for (i = 0;	i<n_buffers[index];	++i){
		LOGINFO("munmap  %d %d : addr %p  len %d\n",index,i,buffers[index][i].start, buffers[index][i].length);
		if (-1 ==	munmap (buffers[index][i].start, buffers[index][i].length))
			LOGINFO ("munmap %d %d error",index,i);
	}
	LOGINFO("free buffer %d",index);
	free(buffers[index]);
	buffers[index]=NULL;
	if(socks[index]!=-1)
		close(socks[index]);
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_main
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint
JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1main(JNIEnv * env, jobject obj,jint index,jstring str)
{
	LOGD("%s with index %d", __func__,index);
	return 0;
}

#if FOR_CAPTURE

void convert_data(int index,unsigned char * data,int len,unsigned char * dstdata)
{
    int i = 0 ,j=0;
    //unsigned int * pdata = (unsigned int *)dstdata;
    memset(dstdata,0,len*3/2);
    for(i=0;i<MYHEIGHT[index]*MYWIDTH[index];i++){
       //unsigned short tdata = 0;
        float rr,bb,gg,yy,uu,vv;
        unsigned char * pb = dstdata + (MYHEIGHT[index] * MYWIDTH[index] -1 - i) * 3;
        yy = data[i*2];
        if(i%2==0){
            uu=data[i*2];
            vv = data[i*2+3];
        }
        else{
            uu = data[i*2-1];
            vv = data[i*2+1];
        }
        rr = yy + (1.370705 * (vv - 128));
        gg = yy - (0.698001 * (vv - 128)) -0.337633 * ( uu -128);
        bb = yy + (1.732446 * (uu - 128));
#define CLIP(x) if(x>255)x=255;else if(x<0)x=0;x=x+0.49
#define CVT(x,y) CLIP(x);y=(unsigned char)floor(x)
        CVT(rr,pb[2]);
        CVT(gg,pb[1]);
        CVT(bb,pb[0]);
        //if(j==0) {
            //tdata = (CVT(rr,5) << 11) | (CVT(gg,6) << 5) | (CVT(bb,5)) ;

            //LOGINFO(" r %f g %f b %f %x", rr, gg, bb,tdata);
           // j == 1;
        //}
        //tdata = (CVT(rr,5) << 11) || (CVT(gg,6) << 5) || (CVT(bb,5)) ;
        //tdata = 0x37e0;

        //*(pdata+i) = *((unsigned int *)pb);
    }
}
#else

void convert_data(int index,unsigned char * data,int len,unsigned char * dstdata)
{
    int i = 0 ,j=0;
    unsigned int * pdata = (unsigned int *)dstdata;
    memset(dstdata,0,len*2);
    for(i=0;i<MYHEIGHT[index]*MYWIDTH[index];i++){
       //unsigned short tdata = 0;
        float rr,bb,gg,yy,uu,vv;
        unsigned char * pb = dstdata + i * 4;
        yy = data[i*2];
        if(i%2==0){
            uu=data[i*2];
            vv = data[i*2+3];
        }
        else{
            uu = data[i*2-1];
            vv = data[i*2+1];
        }
        rr = yy + (1.370705 * (vv - 128));
        gg = yy - (0.698001 * (vv - 128)) -0.337633 * ( uu -128);
        bb = yy + (1.732446 * (uu - 128));
#define CLIP(x) if(x>255)x=255;else if(x<0)x=0;x=x+0.49
#define CVT(x,y) CLIP(x);y=(unsigned char)floor(x)
        CVT(rr,pb[0]);
        CVT(gg,pb[1]);
        CVT(bb,pb[2]);
        //if(j==0) {
            //tdata = (CVT(rr,5) << 11) | (CVT(gg,6) << 5) | (CVT(bb,5)) ;

            //LOGINFO(" r %f g %f b %f %x", rr, gg, bb,tdata);
           // j == 1;
        //}
        //tdata = (CVT(rr,5) << 11) || (CVT(gg,6) << 5) || (CVT(bb,5)) ;
        //tdata = 0x37e0;

        *(pdata+i) = *((unsigned int *)pb);
    }
}
#endif
#if FOR_CAPTURE
#pragma pack(push)
#pragma pack(1)

typedef short WORD;
typedef long DWORD;
typedef long LONG;
typedef char BYTE;
typedef struct
{
    GLbyte	identsize;              // Size of ID field that follows header (0)
    GLbyte	colorMapType;           // 0 = None, 1 = paletted
    GLbyte	imageType;              // 0 = none, 1 = indexed, 2 = rgb, 3 = grey, +8=rle
    unsigned short	colorMapStart;          // First colour map entry
    unsigned short	colorMapLength;         // Number of colors
    unsigned char 	colorMapBits;   // bits per palette entry
    unsigned short	xstart;                 // image x origin
    unsigned short	ystart;                 // image y origin
    unsigned short	width;                  // width in pixels
    unsigned short	height;                 // height in pixels
    GLbyte	bits;                   // bits per pixel (8 16, 24, 32)
    GLbyte	descriptor;             // image descriptor
} TGAHEADER;
typedef struct tagBITMAPFILEHEADER { 
  WORD    bfType; //BMP文件类型，总是字符BM，十六进制为0x4d42
 DWORD   bfSize; //BMP文件大小，包含这个结构在内。
 WORD    bfReserved1; 
  WORD    bfReserved2; //以上均保留为0
 DWORD   bfOffBits; //是一个偏移量，指出了文件中图素位开始位置的字节偏移量
} BITMAPFILEHEADER, *PBITMAPFILEHEADER; 
typedef struct tagBITMAPINFOHEADER{

 DWORD biSize; //结构的大小

 LONG   biWidth; //位图的宽度

 LONG   biHeight; //位图的高度

 WORD   biPlanes; //必须是1

 WORD   biBitCount; //指出每一个像素要用的bit位。

 DWORD biCompression; //指出是否是压缩的，以及压缩方式

 DWORD biSizeImage; //指出图像的尺寸

 LONG   biXPelsPerMeter; //水平基线

 LONG   biYPelsPerMeter; //坚直基线

 DWORD biClrUsed; //被用的颜色数

 DWORD biClrImportant; //重要的颜色数

} BITMAPINFOHEADER, *PBITMAPINFOHEADER; 
typedef struct tagRGBQUAD // rgb
{

           BYTE rgbBlue ;     // blue level

           BYTE rgbGreen ;    // green level

           BYTE rgbRed ;      // red level

           BYTE rgbReserved ; // = 0

}RGBQUAD ;
#pragma pack(pop)

static int filecount[4]={0,0,0,0};
#define ONCELEN	50
int sendToUdp( int ind,unsigned char * yuyvdata,int bufsize,unsigned char * rgbdata)
{
	//int nlen = sizeof(sockaddr);
	//while(bufsize>0){
	//	int sendlen = ONCELEN;
	//	if(bufsize<ONCELEN)
	//		sendlen = bufsize;
		//int ret=sendto(socks[ind], yuyvdata, bufsize, 0, (sockaddr*)&addrto[ind], nlen);
	//	int ret=sendto(socks[ind], yuyvdata, sendlen, 0, (sockaddr*)&addrto[ind], nlen);
	if(socks[ind]==-1)
		return -1;
	int ret = send(socks[ind],yuyvdata,bufsize,0);
	if(ret<0)
	{
		LOGINFO("send %d len %d error %d ",ind,bufsize,ret);
		return -1 ;
	}
		//bufsize-=ONCELEN;
	//}
	return 0;
}
	
int saveToFile(int ind,unsigned char * yuyvdata,int bufsize,unsigned char * rgbdata)
{
	if(NULL == opendir("/mnt/sdcard/yuyv")){
		mkdir("/mnt/sdcard/yuyv",0755);
	}
	if(NULL == opendir("/mnt/sdcard/bmp")){
		mkdir("/mnt/sdcard/bmp",0755);
	}
	if(NULL == opendir("/mnt/sdcard/tga")){
		mkdir("/mnt/sdcard/tga",0755);
	}
	FILE *pFile;                // File pointer
	
    TGAHEADER tgaHeader;		// TGA file header
    unsigned long lImageSize;   // Size in bytes of image
    //GLbyte	*pBits = (GLbyte *)rgbdata;      // Pointer to bits
    //int  iViewport[4];         // Viewport in pixels
    int width = DEFWIDTH;
    int height = DEFHEIGHT; 
	char szFileName[256]={0};
	WORD wbitsCount;//位图中每个像素所占字节数。

   DWORD dwpalettelsize=0;//调色板大小

   DWORD dwbmdibitsize=0,dwdibsize;

   //BITMAP bitmap;//定义了位图的各种的信息。

   BITMAPFILEHEADER bmfhdr;//定义了大小、类型等BMP文件的信息。

   BITMAPINFOHEADER bi;

   //PBITMAPINFOHEADER lpbi;
	sprintf(szFileName,"/mnt/sdcard/tga/%d_%04d.tga",ind,filecount[ind]++);
	if(filecount[ind]>999){
		return -1;
	}
    // How big is the image going to be (targas are tightly packed)
	lImageSize = width * 3 * height;	
	
    // Allocate block. If this doesn't work, go home
    //pBits = (GLbyte *)malloc(lImageSize);
    //if(pBits == NULL)
    //    return 0;
    
    // Initialize the Targa header
    tgaHeader.identsize = 0;
    tgaHeader.colorMapType = 0;
    tgaHeader.imageType = 2;
    tgaHeader.colorMapStart = 0;
    tgaHeader.colorMapLength = 0;
    tgaHeader.colorMapBits = 0;
    tgaHeader.xstart = 0;
    tgaHeader.ystart = 0;
    tgaHeader.width = width;
    tgaHeader.height = height;
    tgaHeader.bits = 24;
    tgaHeader.descriptor = 0;
    
    // Do byte swap for big vs little endian
#if SAVETGA
    // Attempt to open the file
    pFile = fopen(szFileName, "wb");
    if(pFile == NULL)
		{
        //free(pBits);    // Free buffer and return error
        return 0;
		}
	
    // Write the header
    fwrite(&tgaHeader, sizeof(TGAHEADER), 1, pFile);
    
    // Write the image data
    //fwrite(pBits, lImageSize, 1, pFile);
    fwrite(rgbdata,lImageSize,1,pFile);
	
    // Free temporary buffer and close the file
    //free(pBits);    
    fclose(pFile);
#endif
    //fflush();
    sprintf(szFileName,"/mnt/sdcard/yuyv/%d_%04d.yuyv",ind,filecount[ind]);
#if SAVEYUYV
    pFile = fopen(szFileName,"wb");
    if(pFile == NULL){
    	return -2;
    }
    fwrite(yuyvdata,bufsize,1,pFile);
    fclose(pFile);
#endif
    //fflush();
    sprintf(szFileName,"/mnt/sdcard/bmp/%d_%04d.bmp",ind,filecount[ind]);
#if SAVEBMP
    pFile = fopen(szFileName,"wb");
    if(pFile == NULL){
    	return -3;
    }
    wbitsCount = 24;
    dwbmdibitsize=((width*wbitsCount+31)/8)*height;
    bi.biSize = sizeof(BITMAPINFOHEADER);
    bi.biWidth = width;
    bi.biHeight = height;
    bi.biPlanes = 1;
    bi.biBitCount = 24;
    bi.biCompression = 0;
    bi.biClrImportant = 0;
    bi.biClrUsed = 0;
    bi.biSizeImage = 0;
    bi.biYPelsPerMeter = 0 ;
    bi.biXPelsPerMeter = 0;
    bmfhdr.bfType = 0x4d42;
    bmfhdr.bfSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+dwbmdibitsize;
    bmfhdr.bfReserved1 = 0;
    bmfhdr.bfReserved2 = 0;
    bmfhdr.bfOffBits =(DWORD)sizeof(BITMAPFILEHEADER)+(DWORD)sizeof(BITMAPINFOHEADER)+dwpalettelsize;
    fwrite(&bmfhdr,sizeof(bmfhdr),1,pFile);
    fwrite(&bi,sizeof(bi),1,pFile);
    fwrite(rgbdata,lImageSize,1,pFile);
    fclose(pFile);
#endif
    //fflush();
    return 0;
}
#endif
//static int buf_ind_count[UCCOUNT] = {0};
//static int sSkipCount[UCCOUNT] = {1,1,1,1};
static int read_frame (int index)
{
    struct v4l2_buffer buf;
    unsigned int i;
    //LOGINFO("in readFrame %d\n",index);
    CLEAR (buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
		//if(index == 0)
		//	tSkipCount *= 16;
    ioctl (fd[index], VIDIOC_DQBUF, &buf); //出列采集的帧缓冲
    //++(buf_ind_count[index]);
    //if( (buf_ind_count[index]) % tSkipCount == 0){
    	i = buf.bytesused;
    //assert (buf.index < n_buffers);
    //LOGINFO ("buf.index dq is %d,bytesused %d",buf.index,i);
    //fwrite(buffers[buf.index].start, buf.bytesused, 1, file_fd); //将其写入文件中
    //fclose(file_fd);
		//}
    //if((buf_ind_count[index]) % sSkipCount[index] == 0){
    	convert_data(index,(unsigned char *)buffers[index][buf.index].start, buf.bytesused,disbuf[index]);
#if FOR_CAPTURE
#if SENDTONET
			sendToUdp(index,(unsigned char *)buffers[index][buf.index].start, buf.bytesused,disbuf[index]);
#endif
			saveToFile(index,(unsigned char *)buffers[index][buf.index].start, buf.bytesused,disbuf[index]);	

#else
    	drawData(index,disbuf[index],buf.bytesused,MYWIDTH[index],MYHEIGHT[index]);
#endif
    //}
    //else{
    //	unsigned char tdata = *(unsigned char *)buffers[index][buf.index].start;
    //}
    ioctl (fd[index], VIDIOC_QBUF, &buf); //再将其入列
    return 0;
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_demoVideo
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1demoVideo(JNIEnv* env,jobject obj,jint index)
{
	//LOGINFO("%s", __func__);

	fd_set fds;
	struct timeval tv;
	int r;
	
	FD_ZERO (&fds);//将指定的文件描述符集清空
	FD_SET (fd[index], &fds);//在文件描述符集合中增加一个新的文件描述符
	
	/* Timeout. */
	tv.tv_sec = 0  ;
	tv.tv_usec = 20;
	
	r = select (fd[index] + 1, &fds, NULL, NULL, &tv);//判断是否可读（即摄像头是否准备好），tv是定时
	
	if (-1 == r) {
	if (EINTR == errno)
	    return;
		LOGINFO ("select err");
	}
	if (0 == r) {
		//LOGINFO( "select timeout");
		return;
	}
	//LOGINFO("can read");
	//LOGINFO("select has one\n");
	//if(FD_ISSET(fd[index],&fds)){
	read_frame(index);
		//LOGINFO("%d is select\n");
	//}
	
	//unsigned char * drawdata = read_frame ();//如果可读，执行read_frame ()函数，并跳出循环
	//if(drawdata==NULL)
	//    return;
	
	return;
}

/*
 * Class:     nci_fhaha_testcamera_mySurfaceView
 * Method:    doPlayer_setFill
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_nci_fhaha_testcamera_mySurfaceView_doPlayer_1setFill(JNIEnv* env,jobject obj,jint index, jint vi)
{
	LOGD("%s with index %d", __func__,index);

	//sSkipCount[0] = vi;
	//for(int i=1;i<UCCOUNT;i++){
	//	sSkipCount[i]=vi;
	//}
	return;
}
#ifdef __cplusplus
}
#endif