#include <android/log.h>
#include <jni.h>
#include <android/bitmap.h>
//#include <android/surface.h>
#include <gui/Surface.h>
#include <ui/Region.h>
#include <utils/RefBase.h>
#include <cstdio>


#include "jniSurfaceFunc.h"


using  namespace android;
sp<Surface> native_surface[UCCOUNT];
android::Surface::SurfaceInfo info[UCCOUNT];
android::Region dirtyRegion[UCCOUNT];

extern int  MYWIDTH[UCCOUNT];
extern int MYHEIGHT[UCCOUNT];
static jsize len = 0;

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,__VA_ARGS__)
#define LOGINFO(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

static android::Surface* getNativeSurface(JNIEnv* env, jobject jsurface, jint version)
{
    jclass clazz = env->FindClass("android/view/Surface");
    jfieldID field_surface;
    if(version <=8)
    {
        field_surface = env->GetFieldID(clazz, "mSurface", "I");
    }
    else
        field_surface = env->GetFieldID(clazz, ANDROID_VIEW_SURFACE_JNI_ID, "I");

    if (field_surface == NULL)
    {
        return NULL;
    }
    return (android::Surface *) env->GetIntField(jsurface, field_surface);
}
unsigned int fakedata[480]={0};

int setSurface(JNIEnv *env, jobject jsurface, jint version,int index)
{
    native_surface[index] = getNativeSurface(env, jsurface, version);

    if(android::Surface::isValid(native_surface[index]))
    {
        __android_log_print(ANDROID_LOG_INFO, "libjni", "native_surface is valid");
        return 1;
    }
    else
        __android_log_print(ANDROID_LOG_ERROR, "libjni", "native_surface is invalid");

    return 0;
}

void drawData(int index,unsigned char * disdata,int dsize,int width,int height)
{
	//if(index==1)
	//	return;
    dirtyRegion[index].set(android::Rect(0, 0, width, height));
    status_t err = native_surface[index]->lock(&info[index], &dirtyRegion[index]);
    if(err!=0)
    	return;
#if 0
    int sformat;
		char buf[1024]={0};
    switch(info[index].format)
    {
        case PIXEL_FORMAT_RGBA_8888:
            sformat = 8;
            break;
        case PIXEL_FORMAT_RGBX_8888:
            sformat = 9;
            break;
        case PIXEL_FORMAT_BGRA_8888:
            sformat = 4;
            break;
        case PIXEL_FORMAT_RGB_888:
            sformat = 3;
            break;

        case PIXEL_FORMAT_RGB_565:
            sformat = 6;
            break;
        case PIXEL_FORMAT_RGBA_5551:
            sformat = 7;
            break;
        case PIXEL_FORMAT_RGBA_4444:
            sformat = 2;
            break;

        default:
            sformat = -1;
    }
    
    //LOGINFO("width - %d -- height %d-- format %d---Locked -- %d,dsize %d", info.w, info.h, sformat, err,dsize);
#endif
    if(dsize > info[index].w * info[index].h * 4){
        dsize = info[index].w * info[index].h * 4;
    }
    //unsigned char * startaddr = (unsigned char *)info.bits + (index > 1? MYHEIGHT * (MYWIDTH * 2 * 4) : 0) + (index%2? MYWIDTH * 4:0);
    //memset(buf, 0x77, BUFSIZE);
    //for(int i = 0 ; i< MYHEIGHT;i++){
    //	memcpy(startaddr + i * MYWIDTH * 2*4, disdata + i * MYWIDTH * 4,  MYWIDTH * 4);
    //}
    memcpy(info[index].bits,disdata,MYWIDTH[index] * MYHEIGHT[index] * 4);
    //memcpy(info.bits, bufYUV, len);
    native_surface[index]->unlockAndPost();
}
