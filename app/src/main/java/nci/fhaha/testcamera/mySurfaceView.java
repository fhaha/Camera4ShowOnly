package nci.fhaha.testcamera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;

public class mySurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "mengSurfaceView";
    private static final String mylibname = "mengJniNative";
    public native int  doPlayer_setDataSource(int index,String pathname,int width,int height); //设置每个摄像头视频的宽度和高度
    public native int  doVideoRegister(int index,Surface surf);
    public native void doVideoUnregister(int index);
    public native int  doPlayer_prepare(int index);
    public native int  doPlayer_start(int index);
    public native void doPlayer_exit(int index);
    public native int  doPlayer_main(int index,String path);
    public native void doPlayer_demoVideo(int index);
    public native void doPlayer_setFill(int index,int flag);
    private int myViewId = -1;

    public SurfaceHolder                 mholder;
    public Surface                                 mSurface;
    private Context                                mContext;
    private MediaController mMediaController;
    private Thread  mThread;
    private boolean exitflag ;

    static {
        try{
            Log.d(TAG, "now load lib " + mylibname);
            System.loadLibrary(mylibname);
        }catch(UnsatisfiedLinkError e){
            Log.d(TAG, "Couldn't load lib " + mylibname + e.getMessage());
        }
    }

    public mySurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mholder = this.getHolder();//获取holder
        mholder.setFormat(PixelFormat.RGBA_8888);

        mholder.addCallback(this);
        exitflag=false;
        mContext = context;
    }

    public mySurfaceView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        mholder = this.getHolder();//获取holder
        mholder.addCallback(this);
        exitflag=false;
        mContext = context;
        attachMediaController();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub
        Log.d(TAG,"surfaceChanged width" + width + " height " + height);

        mSurface = holder.getSurface();
        int tindex  = getIndex();
        if(tindex<0)
            return;
        doVideoRegister(tindex,mSurface);
        doPlayer_setDataSource(tindex,"null",width,height);
        startCap(getIndex());
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        Log.d(TAG,"surfaceCreated");
        //here test draw
 /*                Canvas canvas = mholder.lockCanvas(null);//获取画布
                 Paint mPaint = new Paint();
                 mPaint.setColor(Color.RED);
                 canvas.drawRect(new RectF(0,0,320,240), mPaint);
                 mholder.unlockCanvasAndPost(canvas);//解锁画布，提交画好的图像
 */
        //                new Thread(new MyThread()).start();
        //holder.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setVideoPath("/dev/video0");

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        Log.d(TAG,"surfaceDestroyed");

        stopCap(getIndex());

        //doVideoUnregister();
        //doPlayer_exit();
        //                mThread.destroy();
    }

    public boolean onTouchEvent(android.view.MotionEvent event) {
        Log.d(TAG,"onTouchEvent");
        if(mMediaController==null)
            return true;
        if(!mMediaController.isShowing()) {
            mMediaController.show(3000);
        }
        return true;
    }

    private void attachMediaController() {
        mMediaController = new MediaController(mContext);
        View anchorView = this.getParent() instanceof View ?
                (View)this.getParent() : this;
        mMediaController.setMediaPlayer(mMediaPlayerControl);
        mMediaController.setAnchorView(anchorView);
        mMediaController.setEnabled(true);
    }

    MediaPlayerControl mMediaPlayerControl = new MediaPlayerControl() {

        @Override
        public boolean canPause() {
            // TODO Auto-generated method stub
            Log.d(TAG,"canPause, now is null");
            return false;
        }

        @Override
        public boolean canSeekBackward() {
            // TODO Auto-generated method stub
            Log.d(TAG,"canSeekBackward, now is null");
            return false;
        }

        @Override
        public boolean canSeekForward() {
            // TODO Auto-generated method stub
            Log.d(TAG,"canSeekForward, now is null");
            return false;
        }

        @Override
        public int getAudioSessionId() {
            return 0;
        }

        @Override
        public int getBufferPercentage() {
            // TODO Auto-generated method stub
            Log.d(TAG,"getBufferPercentage, now is null");
            return 0;
        }

        @Override
        public int getCurrentPosition() {
            // TODO Auto-generated method stub
            Log.d(TAG,"getCurrentPosition, now is null");
            return 0;
        }

        @Override
        public int getDuration() {
            // TODO Auto-generated method stub
            Log.d(TAG,"getDuration, now is null");
            return 0;
        }

        @Override
        public boolean isPlaying() {
            // TODO Auto-generated method stub
            Log.d(TAG,"isPlaying, now is null");
            return false;
        }

        @Override
        public void pause() {
            // TODO Auto-generated method stub
            Log.d(TAG,"pause, now is null");
        }

        @Override
        public void seekTo(int pos) {
            // TODO Auto-generated method stub
            Log.d(TAG,"seekTo, now is null");
        }

        @Override
        public void start() {
            // TODO Auto-generated method stub
            Log.d(TAG,"start, now is null");
        }

    };

    public void setMyId(int mysv1) {
        myViewId = mysv1;
    }

    public void startCap(int i) {
        if(i==-1){
            return;
        }
        Log.d(TAG, "startCap: ");
        if(mThread == null) {
            mThread = new Thread(new MyThread());
            exitflag=false;
        }
        if(!mThread.isAlive()){
            int ret = doPlayer_start(getIndex());
            Log.d(TAG, "startCap: doPlayer_start " + getIndex() + " ret " + ret );
            mThread.start();
        }
    }

    public void stopCap(int i)  {
        if(i<0)
            return;
        exitflag = true;
        if(mThread!=null) {
            try {
                synchronized (mThread) {
                    mThread.join();
                    //mThread.isAlive();
                    //doPlayer_prepare(getIndex());
                    doPlayer_exit(getIndex());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {

                mThread=null;
            }

        }

    }

    class MyThread implements Runnable{

        @Override
        public void run() {
            Log.d(TAG,"MyThread");
            int count = 0;
            try {
                Thread.sleep(500);
                int tindex = getIndex();
                if(tindex <0)
                    return;
                doPlayer_setFill(tindex,8);
                //                                        doPlayer_demoVideo();
                //doPlayer_prepare();
                //int startret = doPlayer_start(tindex);
                //Log.d(TAG," start " + getIndex() + " ret " + startret);
                while(!exitflag){
                    doPlayer_demoVideo(getIndex());
/*
                    Canvas c = null;
                    try
                    {
                        synchronized (mholder)
                        {
                            c = mholder.lockCanvas();//锁定画布，一般在锁定后就可以通过其返回的画布对象Canvas，在其上面画图等操作了。
                            switch(myViewId){
                                case R.id.mysv1:
                                    c.drawColor(Color.RED);
                                    break;
                                case R.id.mysv2:
                                    c.drawColor(Color.BLACK);
                                    break;
                                case R.id.mysv3:
                                    c.drawColor(Color.GRAY);
                                    break;
                                case R.id.mysv4:
                                    c.drawColor(Color.GREEN);
                                    break;
                            }
                            Paint p = new Paint(); //创建画笔
                            p.setColor(Color.YELLOW);
                            Rect r = new Rect(100, 50, 300, 220);
                            c.drawRect(r, p);
                            c.drawText("画面 " + getIndex()  +" 这是第"+(count++)+"秒", 100, 10, p);
                            Thread.sleep(1000);//睡眠时间为1秒
                        }
                    }
                    catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    finally
                    {
                        if(c!= null)
                        {
                            mholder.unlockCanvasAndPost(c);//结束锁定画图，并提交改变。

                        }
                    }

                    */
                    //Thread.sleep(5000);
                    Thread.yield();
                }
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //                                doDrawSurface();
        }
    }

    private int getIndex()
    {
        int retid = -1;
        switch(myViewId){
            case R.id.mysv4:
                ++retid;
            case R.id.mysv3:
                ++retid;
            case R.id.mysv2:
                ++retid;
            case R.id.mysv1:
                ++retid;
            default:
                break;
        }
        return retid;
    }

    public void setVideoPath(String filePath) {
    }
}
