package nci.fhaha.testcamera;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends Activity {
    private final static int UCCOUNT = 4;
    private File fileName = null;
    private Button button;
    private TextView tv;
    private ImageView imageView;
    private mySurfaceView myviews[];

    //public native String getV4l2Desc();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(new mySurfaceView(this));
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
      //          WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //设置无标题

       // getWindow().setFlags(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);  //设置全屏

        // 把Activity的标题去掉
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_my_surface_view);
        myviews = new mySurfaceView[UCCOUNT];
        for(int i = 0 ;i<UCCOUNT;i++){
            int tid;
            switch(i){
                case 0:
                    tid = R.id.mysv1;
                    break;
                case 1:
                    tid = R.id.mysv2;
                    break;
                case 2:
                    tid = R.id.mysv3;
                    break;
                case 3:
                    tid = R.id.mysv4;
                    break;
                default:
                    tid = R.id.mysv1;
                    break;
            }
            myviews[i]=(mySurfaceView)findViewById(tid);
            myviews[i].setMyId(tid);
        }
        return ;
/*
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.button);
        tv = (TextView)findViewById(R.id.textView);
        tv.setText(FHJ.V4L2("haha"));
        imageView = (ImageView)findViewById(R.id.imageView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File filePath = new File(Environment.getExternalStorageDirectory(), "myCamera");
                if(!filePath.exists()){
                    filePath.mkdirs();
                }
                fileName = new File(filePath, System.currentTimeMillis() + ".jpg");
                try{
                    if(!fileName.exists()){
                        fileName.createNewFile();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                // intent用来启动系统自带的Camera
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // 将系统Camera的拍摄结果写入到文件
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
                // 启动intent对应的Activity，返回默认消息
                startActivityForResult(intent, Activity.DEFAULT_KEYS_DIALER);
            }
        });
*/
    }

    @Override
    protected void onStop() {
        //for(int i = 0 ;i<UCCOUNT;i++){
        //    myviews[i].stopCap(i);
        //    myviews[i].doPlayer_exit(i);
        //}
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //for(int i = 0 ;i<UCCOUNT;i++){
        //    myviews[i].doPlayer_start(i);
        //    myviews[i].startCap(i);
        //}
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == Activity.DEFAULT_KEYS_DIALER){
            // MainActivity接收Camera返回的消息，然后将已经写入的图片显示在ImageView内
            imageView.setImageURI(Uri.fromFile(fileName));
        }
    }
}
